#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "bmp.h"
#include "util.h"
#include "image_rot_stub.h"


void usage() {
    fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n"); 
}

int main( int argc, char** argv ) {

    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments \n" );
    if (argc > 2) err("Too many arguments \n" );

    /* Open file */
    FILE *f = fopen(argv[1], "rb");
    if (!f)
        err("Failed to open BMP file: %s\n", argv[1]);

    /* Read bmp image */
    struct image src_img = {0};
    enum read_status ret = from_bmp(f, &src_img);
    if (ret != READ_OK){
        fclose(f);
        err( "Failed to read image (err: %d).\n", ret );    
    }
    fclose(f);
    f = NULL;

    /* Rotate source image and fill template */
    struct image rot_img = image_rotate(&src_img);

    /* Save bmp header and image with another file name */
    char filename[512] = {0};
    size_t len = sizeof(filename);
    mix_str(filename, len, "new_", argv[1]);
    f = fopen(filename ,"wb");
    enum write_status ws = to_bmp(f, &rot_img);
    if (ws != WRITE_OK) {
        fclose(f);
        err( "Failed to write image (err: %d).\n", ws);
    }

    printf("Rotated image succesfully written to file: %s\n", filename);
    fclose(f);  

    return 0;
}
