#ifndef __IMAGE_ROT_STUB_H__
#define __IMAGE_ROT_STUB_H__
#include <stdint.h>
#include <stdio.h>

struct image image_rotate(struct image const *source);
#endif /* __IMAGE_ROT_STUB_H__ */
