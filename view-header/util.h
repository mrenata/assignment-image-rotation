#ifndef _UTIL_H_
#define _UTIL_H_

_Noreturn void err( const char* msg, ... );
void mix_str(char* buf, size_t buflen, char* s1, char* s2);
#endif
