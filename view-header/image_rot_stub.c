#include <stdint.h>
#include <stdlib.h>
#include "image_rot_stub.h"
#include "bmp.h"
#include "util.h"

struct image image_rotate(struct image const *source){
    struct pixel pxl;
    uint64_t width = source->width;
    uint64_t height = source->height;

    struct image finl = {
    .width = source->width,
    .height = source->height,
    .data = malloc(width * height * sizeof(struct pixel))
    };
    if (finl.data == NULL) {
        err("error in image rotation");
    }
    finl.width = height;
    finl.height = width;
    for (uint64_t row = 0; row < height; row++) {
        for (uint64_t col = 0; col < width; col++) {
            pxl = source->data[width * row + col];
            finl.data[height * col + height - row - 1] = pxl;
        }
    }
    return finl;
}

