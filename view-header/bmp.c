#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "bmp.h"
#include "util.h"
#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))
#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");
#define BM 0x4D42
#define FOR_BMP_HEADER( FOR_FIELD ) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)
#define DECLARE_FIELD( t, n ) t n ;

struct __attribute__((packed)) bmp_header
{
   FOR_BMP_HEADER( DECLARE_FIELD )
};

static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

struct bmp_header create_header(struct image const* img){
    uint64_t width = img->width;
    uint64_t height = img->height;
    struct bmp_header my_bmp = {0};
    my_bmp.bfType = BM;
    my_bmp.bfileSize = 54 + (3 * width ) % 4 * height;
    my_bmp.bfReserved = 0;
    my_bmp.bOffBits = sizeof(struct bmp_header);
    my_bmp.biSize = 40;
    my_bmp.biWidth = width;
    my_bmp.biHeight = height;
    my_bmp.biPlanes = 1;
    my_bmp.biBitCount = 24;
    my_bmp.biCompression = 0;
    my_bmp.biSizeImage = my_bmp.bfileSize - my_bmp.bOffBits;
    my_bmp.biXPelsPerMeter = 0;
    my_bmp.biYPelsPerMeter = 0;
    my_bmp.biClrUsed = 0;
    my_bmp.biClrImportant = 0;
    return my_bmp;
}

static enum read_status read_header_from_file( FILE* f, struct bmp_header * header ) {
    enum read_status ret = READ_OK;
    if (!read_header( f, header ) ) {
        return READ_ERROR;
    }
    if (header->bfType != BM)
        ret = READ_INVALID_HEADER;
    else if (header->biBitCount != 24)
        ret = READ_INVALID_BITS;

    return ret;
}

enum read_status from_bmp(FILE* in, struct image* read){
        struct bmp_header h = {0};
        enum read_status ret = read_header_from_file( in, &h );
        if (ret != READ_OK) {
            err( "Failed to reading header (err: %d).\n", ret );
        }
        read->width = h.biWidth;
        read->height = h.biHeight;
        read->data = (struct pixel*)calloc(read->width * read->height, sizeof(struct pixel));
        if (read->data == NULL){
            err("Failed to allocate memory for image data");
        }
        const uint32_t width = h.biWidth;
        const uint32_t height = h.biHeight;
        for (size_t i = 0; i < height; i++) {
            const size_t count = width * i;
            const size_t img_reader = fread(&read->data[count], sizeof(struct pixel), width, in);
            if (img_reader != width) {
                return READ_INVALID_BITS;
            }
            uint32_t checker = ( 3 * width ) % 4;
            for (uint32_t j = 0; j < checker; j++) {
                uint8_t tmp = 0;
                if (fread(&tmp, sizeof(uint8_t), 1, in) != 1) {
                    return READ_INVALID_BITS;
                }
            }
        }
    return READ_OK;
}

static enum write_status write_header( FILE* file, struct image const* img){
    struct bmp_header h = create_header(img);
    size_t tmp = fwrite(&h, sizeof(struct bmp_header), 1, file);
    if (tmp != 1){
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img ){
    const uint32_t width = img->width;
    const uint32_t height = img->height;    
    enum write_status ws = write_header(out, img);
    if (ws != WRITE_OK) {
        fclose(out);
        err( "Failed to write header (err: %d).\n", ws);
    }
   for (size_t i = 0; i < height; i++) {
        const size_t count = width * i;
        const size_t img_reader = fwrite(&img->data[count], sizeof(struct pixel), width, out);
        if (img_reader != width) {
            return WRITE_ERROR;
        }
        uint32_t checker = ( 3 * width ) % 4;
        for (uint32_t j = 0; j < checker; j++) {
            uint8_t tmp = 0;
            if (fwrite(&tmp, sizeof(uint8_t), 1, out) != 1) {
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}
