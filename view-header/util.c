#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include "util.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args);
  va_end (args);
  exit(1);
}

void mix_str(char* buf, size_t buflen, char* s1, char* s2){
    if ((strlen(buf) + strlen(s1) + strlen(s2) + 1) > buflen){
        err("Too big string");
    }
    strcat(buf, s1);
    strcat(buf, s2);
    if (buf == NULL){
        err("buf must not be null");
    }
}
